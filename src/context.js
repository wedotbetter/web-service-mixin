import Request  from './request';
import Response from './response';
import Debug    from 'debug';

const debug = Debug('web-service');

export default class WebSocketContext {

    constructor(client, method, path, body, meta) {
        this.respond  = true;
        this.client   = client;
        this.state    = {};
        this.method   = method;
        this.path     = path;
        this.request  = new Request(client.request, body);
        this.response = new Response();
        this.meta     = meta;
    }

    get body() {
        return this.response.body;
    }

    set body(v) {
        this.response.body = v;
    }

    get writable() {
        return this.response.writable;
    }

    set writable(w) {
        this.response.writable = w;
    }

    get header() {
        return this.request.headers;
    }

    get headers() {
        return this.request.headers;
    }

    get url() {
        return this.request.url;
    }

    set url(v) {
        this.request.url = v;
    }

    get host() {
        return this.request.host;
    }

    get hostname() {
        return this.request.hostname;
    }

    get charset() {
        return this.request.charset;
    }

    get length() {
        return this.response.length;
    }

    set length(l) {
        this.response.length = l;
    }

    get ips() {
        return this.request.ips;
    }

    get message() {
        return this.response.message;
    }

    set message(msg) {
        this.response.message = msg;
    }

    get status() {
        return this.response.status;
    }

    set status(code) {
        this.response.status = code;
    }

    get type() {
        return this.response.type;
    }

    set type(type) {
        this.response.type = type;
    }

    get socket() {
        return this.request.socket;
    }

    get protocol() {
        return this.request.protocol;
    }

    get secure() {
        return this.request.secure;
    }

    get(field) {
        return this.request.get(field);
    }

    emit(...args) {
        this.client.emit(...args);
    }

    end(...args) {
        if (!this.writable)
            return;
        this.writable = false;
        if (args.length)
            this.client.emit(...args);
    }
}
