import _         from 'lodash';
import Promise   from 'bluebird';
import Namespace from 'socket.io/lib/namespace';
import Socket    from 'socket.io/lib/socket';
import Debug     from 'debug';
import SocketIo  from 'socket.io';
import compose   from 'koa-compose';

import Context   from './context';

const debugWS = Debug('web-service');
const debugIO = Debug('socket.io:socket');


Socket.prototype.join = function(room, cb) {

    if (_.isUndefined(room))
        return this;

    debugWS(`${this.id} entering ${room == this.id ? '' : room}`);

    if (!room.startsWith('/'))
        room = '/'+room;

    const rooms = [room];
    const roomLayers = room.split('/');

    for (var j = 0; j < roomLayers.length; j++) {
        roomLayers.pop();
        room = roomLayers.join('/');
        room = room ? room : '/';
        rooms.push(room);
    }

    debugWS('joining rooms %s', rooms);

    const addToAdapterPromise = Promise.promisify(this.adapter.add, { context : this.adapter });
    let promises = [];
    for (let i = 0; i < rooms.length; i++) {
        let _room = rooms[i];
        if (this.rooms.hasOwnProperty(_room))
            continue;
        promises.push(addToAdapterPromise(this.id, _room));
    }

    if (_.isFunction(cb)) {
        Promise.all(promises).finally(cb);
    }

    return this;
}

Socket.prototype.dispatch = function(packet) {
    debugWS('dispatching an packet %j', packet);
    var self = this;
    this.run(packet, function(err) {
        if (err) {
            let errorEvent = _.isString(err.name) ? err.name : 'error';
            return self.emit(errorEvent, {
                status  : err.status || 500,
                message : err.message || 'Internal Error',
                stack   : err.stack
            });
        }
        self.emit.apply(self, packet);
    });
}

Socket.prototype.run = function(packet, fn) {
    let middlewares = this.fns;
    let handler = compose(middlewares);

    let [ event, payload, meta ] = packet;
    let [ method, path = '' ] = event.split(' ');

    debugWS('<- %s %s %s', method.toUpperCase(), path, JSON.stringify(payload));

    let ctx = new Context(this, method, path, payload, meta);

    handler(ctx)
        .then(() => {
            if (false === ctx.respond) return;

            if (!ctx.writable)
                return;

            let responseEvent = ctx.meta.responseEvent || 'response:'+event;

            if (null == ctx.body) {
                ctx.type = 'text';
                ctx.body = ctx.message || String(ctx.status);
                ctx.length = Buffer.byteLength(ctx.body);
                return ctx.end(responseEvent, ctx.response);
            }

            if (!Buffer.isBuffer(ctx.response.body) && 'string' != typeof ctx.response.body)
                ctx.response.length = Buffer.byteLength(JSON.stringify(ctx.response.body));

            ctx.end(responseEvent, ctx.response);
            return;
        })
        .catch(fn);

};

export default function(...args) {
    return new SocketIo(...args);
}
