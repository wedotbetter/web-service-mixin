import statuses        from 'statuses';
import { contentType } from 'mime-types';

export default class Response {
    constructor() {
        this.writable = true;
        this._body    = null;
    }

    get status() {
        return this._status;
    }

    set status(code) {
        this._status         = code;
        this._explicitStatus = true;
        this._statusMessage  = statuses[code];
        if (this.body && statuses.empty[code])
            this.body = null;
    }

    get message() {
        return this._statusMessage || statuses[this.status];
    }

    set message(msg) {
        this._statusMessage = msg;
    }

    get body() {
        return this._body;
    }

    set body(v) {
        let body = this._body;
        this._body = v;

        if (null == v) {
            if (!statuses.empty[this.status])
                this.status = 204;
            return;
        }

        if (!this._explicitStatus)
            this.status = 200;

        if ('string' == typeof val) {
            if (!this.type)
                this.type = /^\s*</.test(val)
                    ? 'html'
                    : 'text';
            this.length = Buffer.byteLength(val);
            return;
        }

        if (Buffer.isBuffer(val)) {
            if (setType)
                this.type = 'bin';
            this.length = val.length;
            return;
        }

        this.type = 'json';
    }

    get length() {
        if (this._explicitLength)
            return this._length;
        let body = this.body;
        if (!body)
            return;
        if ('string' == typeof body)
            return Buffer.byteLength(body);
        if (Buffer.isBuffer(body))
            return body.length;
        if (isJSON(body))
            return Buffer.byteLength(JSON.stringify(body));
        return;
    }

    set length(l) {
        this._explicitLength = true;
        this._length = l;
    }

    get type() {
        return this._type;
    }

    set type(type) {
        this._type = contentType(type);
    }

    toJSON() {
        return {
            status  : this.status,
            message : this.message,
            type    : this.type,
            length  : this.length,
            body    : this.body
        }
    }
}

function isJSON(target) {
    try {
        JSON.stringify(target);
        return true;
    } catch(err) {
        return false;
    }
}
