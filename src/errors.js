import custerror from 'custerror';

export const WebServiceError = custerror('WebServerError');
export const SocketError     = custerror('SocketError');
export const DecoratorError  = custerror('DecoratorError');
export const Unauthorized    = custerror('AuthenticationError', { status : 402 });
