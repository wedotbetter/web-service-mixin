import _              from 'lodash';
import Promise        from 'bluebird';
import pathToRegexp   from 'path-to-regexp';
import minimatch      from 'minimatch';
import Debug          from 'debug';
import util           from 'util';
import { memorize }   from 'memorize';

import parseOptions, { parseArguments } from 'args-options';

import { DecoratorError, Unauthorized } from '../errors';

const debug = Debug('web-service');

const defaults = {
    methods      : [],
    routePattern : '',
    paths        : [],
    protocol     : ['http', 'https'],
    order        : 0,
    prefixes     : [],
    suffix       : ''
};

export default class RouteClass {
    constructor(...args) {
        let options = parseArguments(['methods', 'routePattern', 'paths'], args);
        if (options.routePattern) {
            let patternParts = pathToRegexp.parse(options.routePattern);
            options.paramKeys = options.paramKeys || [];
            _.forEach(patternParts, part => {
                if (_.isString(part.name))
                    options.paramKeys.push(part.name);
            });
        }
        this.options = _.merge({}, defaults, options);
    }

    @parseOptions(['methods', 'routePattern', 'paths'])
    Route(options) {
        let parentOptions = _.cloneDeep(this.options);
        let { routePattern } = parentOptions;

        if (!routePattern.endsWith('/:x*'))
            routePattern = routePattern+'/:x*';

        options.prefixes = [...(parentOptions.prefixes || []), routePattern ];
        return new RouteClass(_.merge({}, parentOptions, options));
    }

    ALL(...args) {
        return this.Route('{GET,POST,PUT,DELETE}', ...args);
    }

    GET(...args) {
        return this.Route('GET', ...args);
    }

    POST(...args) {
        return this.Route('POST', ...args);
    }

    PUT(...args) {
        return this.Route('PUT', ...args);
    }

    DELETE(...args) {
        return this.Route('DELETE', ...args);
    }

    matchProtocol(protocol) {
        return !protocol || ~_.indexOf(this.options.protocol, protocol);
    }

    matchMethod(method) {
        let { methods } = this.options;
        methods = _.isString(methods) ? methods.split(' ') : methods;
        if (methods.length) {
            return _.reduce(methods, (result, acceptedMethod) => {
                return method[0] === '!'
                    ? result && minimatch(method, acceptedMethod)
                    : result || minimatch(method, acceptedMethod)
            }, methods[0][0] === '!');
        }
        return true;
    }

    matchPath(path) {
        let { paths } = this.options;
        paths = _.isString(paths) ? paths.split(' ') : paths;
		if (paths.length) {
            return _.reduce(paths, (result, matchPath) => {
                return matchPath[0] === '!'
                    ? result && minimatch(path, matchPath)
                    : result || minimatch(path, matchPath)
            }, paths[0][0] === '!');
        }
        return true;
    }

    matchPattern(path) {
        let routePattern = this.options.routePattern;
        if (routePattern) {
            const re = pathToRegexp(routePattern);
            return re.exec(path);
        }
        return [];
    }

    createGetter(detail, origGetter) {
        let self = this;
        return function() {
            let origHandler = origGetter.call(this);
            return self.createHandler(detail, origHandler);
        }
    }

    createHandler(detail, origHandler) {
        let self = this;

        let { pre, preContext, post, postContext, context } = detail;

        return async function(ctx, next) {
            let path = ctx.path;
            debug(`------------------------------------`);
            debug(`Matching route`);
            debug(`------------------------------------`);
            debug(util.inspect(self, { depth : null }));
            if (!self.matchProtocol(ctx.protocol)) {
                debug(`Invalid protocol`);
                return next();
            }
            if (!self.matchMethod(ctx.method)) {
                debug(`Method not match, ${ctx.method}`);
                return next();
            }
            let params = [];
            for (let i in self.options.prefixes) {
                try {
					let prefix = self.options.prefixes[i];
					let prefixPattern = pathToRegexp(prefix).exec(path);
					if (!prefixPattern) {
						debug(`Prefix not match, ${path}`);
						return next();
					}
					let remain = prefixPattern.pop();
					path = remain ? '/'+remain : '';
					params = [ ...prefixPattern.slice(1) ];
                } catch(err) {
                    debug(err);
                }
            }

            if (!self.matchPath(path)) {
                debug(`Path not match, ${path}`);
                return next();
            }
            let pattern = self.matchPattern(path);
            if (!pattern) {
                debug(`Pattern noth match, ${path}`);
                return next();
            }

            debug(`------------------------------------`);
            debug(`Running handler`);
            params = [ ...params, ...pattern.slice(1, pattern.length) ];

            ctx.state.routeParams = _.zipObject(self.options.paramKeys, params);

            let args = await Promise.resolve(pre ? pre.call(preContext || this, ctx, next) : [ctx, next]);

            let handlerReturn = await Promise.resolve(origHandler.apply(context || this, args));

            var result = await Promise.resolve(post ? post.call(postContext || this, ctx, handlerReturn, next) : handlerReturn);

            debug(`------------------------------------`);

            return result;
        }
    }

    @parseOptions([['REST.pre', 'realtime.pre'], ['REST.preContext', 'realtime.preContext']])
    pre(options) {
        this.options = _.merge(this.options, options)
        return this;
    }

    @parseOptions([['REST.post', 'realtime.post'], ['REST.postContext', 'realtime.postContext']])
    post(options) {
        this.options = _.merge(this.options, options)
        return this;
    }

    realtime(options = {}) {
        this.options.realtime = options;
        return this;
    }

    REST(options = {}) {
        this.options.REST = options;
        return this;
    }

    http(enable) {
        let { options } = this;

        if (enable !== false) {
            options.protocol = _.union(options.protocol, ['http']);
            return this;
        }
        _.pull(options.protocol, 'http');
        return this;
    }

    https(enable) {
        let { options } = this;

        if (enable !== false) {
            options.protocol = _.union(options.protocol, ['https']);
            return this;
        }
        _.pull(options.protocol, 'https');
        return this;
    }

    suffix(suffix) {
        this.options.suffix = suffix;
        return this;
    }

    order(order) {
        this.options.order = order;
        return this;
    }

    use() {
        let self = this;
        debug(util.inspect(this.options, { depth : null }));
        return function(proto, key, descriptor) {
            let { options : { suffix } } = self;
            if (!self.options.realtime && !self.options.REST)
                throw new DecoratorError('Can not build without route handler');

            let { initializer, value, get } = descriptor;

            let newDescriptor = {
                ...descriptor
            }

            _.forEach(['realtime', 'REST'], type => {
                let detail = self.options[type];
                if (detail) {
                    let propType, fn;
                    if (initializer) {
                        propType = 'get';
                        fn       = memorize()(self.createGetter(detail, initializer));
                        delete newDescriptor.writable;
                        delete newDescriptor.initializer;
                    }
                    if (get) {
                        propType = 'get';
                        fn       = memorize()(self.createGetter(detail, get));
                    }
                    if (value) {
                        propType = 'value';
                        fn       = self.createHandler(detail, value);
                    }

                    newDescriptor[propType] = fn;
                    let routeKey = `${type}:${key}${suffix}`;
                    Object.defineProperty(proto, routeKey, newDescriptor);
                    Object.defineProperty(proto, `${routeKey}:order`, {
                        value : self.options.order
                    });
                }
            });

            return descriptor;
        }
    }
}

export const Route = function(...args) {
    return new RouteClass(...args);
}

export const ALL = function(...args) {
    return Route('{GET,POST,PUT,DELETE}', ...args);
}

export const GET = function(...args) {
    return Route('GET', ...args);
}

export const POST = function(...args) {
    return Route('POST', ...args);
}

export const PUT = function(...args) {
    return Route('PUT', ...args);
}

export const DELETE = function(...args) {
    return Route('DELETE', ...args);
}


const _parseList = str => _.isArray(str) ? str : (_.isString(str) ? str.split(' ') : [] );
