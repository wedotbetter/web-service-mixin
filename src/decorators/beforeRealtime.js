import _ from 'lodash';
import { memorize } from 'memorize';

export default function BeforeRealtime() {
    return new BeforeRealtimeClass();
}

export const BeforeRealtimeClass = class BeforeRealtime {

    constructor() {
        this.options = {
            order  : 0,
            suffix : ''
        };
    }

    config(options) {
        this.options = _.merge(this.options, options);
        return this;
    }

    order(order) {
        this.options.order = order;
        return this;
    }

    context(context) {
        this.options.context = context;
    }

    suffix(suffix) {
        this.options.suffix = suffix;
        return this;
    }

    use() {
        let self = this;
        return function(proto, key, descriptor) {

            let { initializer, value, get } = descriptor;
            let { context, suffix } = self.options;
            let propType, fn;

            let newDescriptor = {
                ...descriptor
            };

            if (initializer) {
                propType = 'get';
                fn = memorize()(function() {
                    let handler = initializer.call(this);
                    if (context)
                        return handler.bind(context);
                    return handler;
                });
                delete newDescriptor.initializer;
                delete newDescriptor.writable;
            }

            if (get) {
                propType = 'get';
                fn = memorize()(function() {
                    let handler = get.call(this);
                    if (context)
                        return handler.bind(context);
                    return handler;
                });
            }

            if (value) {
                propType = 'value';
                fn = context ? value.bind(context) : value;
            }

            newDescriptor[propType] = fn;

            let middlewareKey = `beforeRealtime:${key}${suffix}`;
            Object.defineProperty(proto, middlewareKey, newDescriptor);
            Object.defineProperty(proto, `${middlewareKey}:order`, {
                value : self.options.order
            });

            return descriptor;
        }
    }
}
