import contentType from 'content-type';

export default class Request {
    constructor(req, body) {
        this.req  = req;
        this.body = body;
    }

    get header() {
        return this.req.header;
    }

    get header() {
        return this.req.headers;
    }

    get headers() {
        return this.req.headers;
    }

    get url() {
        return this.req.url;
    }

    set url(v) {
        this.req.url = v;
    }

    get host() {
        let host = this.get('Host');
        if (!host) return '';
        return host.split(/\s*,\s*/)[0];
    }

    get hostname() {
        const host = this.host;
        if (!host) return '';
        return host.split(':')[0];
    }

    get charset() {
        const type = this.get('Content-Type');
        if (!type) return '';
        return contentType.parse(type).parameters.charset || '';
    }

    get length() {
        const len = this.get('Content-Length');
        if (len == '') return;
        return ~~len;
    }

    get ips() {
        const val = this.get('X-Forwarded-For');
        return val
            ? val.split(/\s*,\s*/)
            : [];
    }

    get socket() {
        return this.req.socket;
    }

    get protocol() {
        if (this.socket.encrypted) return 'https';
        const proto = this.get('X-Forwarded-Proto') || 'http';
        return proto.split(/\s*,\s*/)[0];
    }

    get secure() {
        return 'https' == this.protocol;
    }

    get(field) {
        const req = this.req;
        switch (field = field.toLowerCase()) {
            case 'referer':
            case 'referrer':
                return req.headers.referrer || req.headers.referer || '';
            default:
                return req.headers[field] || '';
        }
    }
}
