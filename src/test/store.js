export default {
    users : {
        '1' : {
            username : 'paul'
        },
        '2' : {
            username : 'tom'
        },
        '3' : {
            username : 'john'
        }
    },
    projects : {
        '1' : {
            names : 'route-decorator',
            reports : {
                '1' : {
                    content : 'release'
                },
                '2' : {
                    content : 'great success'
                }
            },
            files : {
                '1' : {
                    name : 'image'
                },
                '2' : {
                    name : 'document'
                }
            }
        },
        '2' : {
            names : 'web-service-mixin',
            reports : {
                '1' : {
                    content : 'release'
                },
                '2' : {
                    content : 'also great success'
                }
            },
            files : {
                '1' : {
                    name : 'spreadsheet'
                },
                '2' : {
                    name : 'slides'
                }
            }
        }
    }
}
