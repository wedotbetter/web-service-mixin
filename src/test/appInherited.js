import { mix }     from 'mixwith';
import socketioJwt from 'socketio-jwt';

import { WebServiceMixin, Route, BeforeRealtime } from '../index';
import Controller from './controller';

const handledRoute = ['users', 'issues'];

export default class App extends mix(class {}).with(WebServiceMixin, Controller) {

    constructor(config) {
        super();
        this.context = 'app';
        this.config(config)
            .import(this)
            .initWebService();
    }

    @BeforeRealtime().use()
    method(client, next) {
        client.state = client.state || {}
        client.state.method = {
            token : (client.request.query || client.request._query).token
        };
        return next();
    }

    @BeforeRealtime().use()
    initializer = (client, next) => {
        client.state = client.state || {}
        client.state.initializer = {
            token : (client.request.query || client.request._query).token
        };
        return next();
    }

    @BeforeRealtime().use()
    get getter() {
        return function(client, next) {
            client.state = client.state || {}
            client.state.getter = {
                token : (client.request.query || client.request._query).token
            };
            return next();
        }
    }

    @Route('GET', '/beforeConnectState').realtime().use()
    getBeforeConnectState(ctx, next) {
        ctx.body = ctx.client.state;
        return next();
    }
}
