import ioClient from 'socket.io-client';
import assert   from 'assert';
import fetch    from 'node-fetch';
import Debug    from 'debug';
import path     from 'path';
import https    from 'https';

const debug = Debug('web-service');

import AppInherited  from './appInherited';
import AppComposited from './appComposited';
import { ssl }       from '../index';
import store         from './store';

async function getConfigs() {
	let cred = await ssl(path.join(__dirname, 'ssl'), { selfSigned : true });
	return {
		http : {
			port : 1345,
			cb : () => debug('[HTTP] Server started, listening request at 1345')
		},
		https : {
			cred,
			port : 13456,
			cb : () => debug('[HTTPS] Server started, listening request at 13456')
		},
		REST     : {},
		realtime : {},
		redis    : {}
	}
}

describe('web-service-mixin(inheritance)', function() {
	let app, ssl;
	before('should configuring app and starting service without error', async function() {
		let configs = await getConfigs();
		app = new AppInherited(configs);
		app.startWebService();
	});
	describe('Realtime service', function(configs) {
		const token = 'dlkasjdafhf3r920u39r0jowifj920f';
		let client, anotherClient;
		it('should allow user to connect', function(done) {
			client = ioClient('https://localhost:13456', { query : `token=${token}`, rejectUnauthorized: false});
			client.on('connect', function() {
				done();
			});
		});

		it('should pass query', function(done) {
			client.on('response:beforeConnect', res => {
				assert.deepEqual(res.body, {
					method : { token },
					initializer : { token },
					getter : { token }
				});
				done();
			});
			client.emit('GET /beforeConnectState', {}, { responseEvent : 'response:beforeConnect' });
		});

		it('should allow client to join room', function(done) {
			client.on('response:joinRoom', res => {
				assert.deepEqual(res.body, {
					success : 1
				});
				done();
			});
			client.emit('joinRoom', { room : '/project:1/report:1' }, { responseEvent : 'response:joinRoom' });
		});

		it('should allow another client to join room', function(done) {
			anotherClient = ioClient('https://localhost:13456', { query : `token=${token}`, rejectUnauthorized: false});
			anotherClient.on('connect', function() {
				anotherClient.emit('joinRoom', { room : '/project:1/report:1' }, { responseEvent : 'response:joinRoom' });
			});
			anotherClient.on('response:joinRoom', res => {
				assert.deepEqual(res.body, {
					success : 1
				});
				done();
			});
		});

		it('should boardcast message to both clients', function(done) {
			let clientReceive = new Promise((resolve, reject) => {
				client.on('broadcast', res => {
					assert.deepEqual(res.body, {
						success : 1
					});
					resolve();
				});
			});
			let anotherClientReceive = new Promise((resolve, reject) => {
				anotherClient.on('broadcast', res => {
					assert.deepEqual(res.body, {
						success : 1
					});
					resolve();
				});
			});

			Promise.all([clientReceive, anotherClientReceive]).then(() => done());

			client.emit('broadcast');
		});

		it('should allow client access to REST API', function(done) {
			client.on('response:getUser', res => {
				assert.deepEqual(res.body, {
					context : 'app',
					user : store.users['1']
				});
				done();
			});
			client.emit('GET /admin/users/1', {}, { responseEvent : 'response:getUser'});
		});

		it('should able to catch error', function(done) {
			client.on('Error', res => {
				assert.equal(res.status, 500);
				done();
			});
			client.emit('testError');
		})

	});
	describe('REST API', function() {
		it('render view', function() {
			return fetch('https://localhost:13456/testView', {
				method : 'GET',
				agent : new https.Agent({
					rejectUnauthorized : false
				})
			}).then(res => {
				assert.equal(200, res.status);
				return res;
			});
		});
		it('GET admin/users', function() {
			return fetch('https://localhost:13456/admin/users', {
				method : 'GET',
				agent : new https.Agent({
					rejectUnauthorized : false
				})
			}).then(res => {
				assert.equal(200, res.status);
				return res.json();
			}).then(json => {
				assert.deepEqual(json, store.users)
			});
		});
		it('GET /projects/2/reports/2', function() {
			return fetch('https://localhost:13456/projects/2/reports/2', {
				method : 'GET',
				agent : new https.Agent({
					rejectUnauthorized : false
				})
			}).then(res => {
				assert.equal(200, res.status);
				return res.json();
			}).then(json => {
				assert.deepEqual(json, store.projects['2'].reports['2']);
			});
		});

		it('GET /projects/2/files/1', function() {
			return fetch('https://localhost:13456/projects/2/files/1', {
				method : 'GET',
				agent : new https.Agent({
					rejectUnauthorized : false
				})
			}).then(res => {
				assert.equal(200, res.status);
				return res.json();
			}).then(json => {
				assert.deepEqual(json, store.projects['2'].files['1']);
			});
		});
	});
	after(function() {
		app.stopWebService();
	});
});

describe('web-service-mixin(composited)', function() {
	let app, ssl;
	before('should configuring app and starting service without error', async function() {
		let configs = await getConfigs();
		app = new AppComposited(configs);
		app.webService.startWebService();
	});
	describe('Realtime service', function(configs) {
		const token = 'dlkasjdafhf3r920u39r0jowifj920f';
		let client, anotherClient;
		it('should allow user to connect', function(done) {
			client = ioClient('https://localhost:13456', { query : `token=${token}`, rejectUnauthorized: false});
			client.on('connect', function() {
				done();
			});
		});

		it('should pass query', function(done) {
			client.on('response:beforeConnect', res => {
				assert.deepEqual(res.body, {
					method : { token },
					initializer : { token },
					getter : { token }
				});
				done();
			});
			client.emit('GET /beforeConnectState', {}, { responseEvent : 'response:beforeConnect' });
		});

		it('should allow client to join room', function(done) {
			client.on('response:joinRoom', res => {
				assert.deepEqual(res.body, {
					success : 1
				});
				done();
			});
			client.emit('joinRoom', { room : '/project:1/report:1' }, { responseEvent : 'response:joinRoom' });
		});

		it('should allow another client to join room', function(done) {
			anotherClient = ioClient('https://localhost:13456', { query : `token=${token}`, rejectUnauthorized: false});
			anotherClient.on('connect', function() {
				anotherClient.emit('joinRoom', { room : '/project:1/report:1' }, { responseEvent : 'response:joinRoom' });
			});
			anotherClient.on('response:joinRoom', res => {
				assert.deepEqual(res.body, {
					success : 1
				});
				done();
			});
		});

		it('should boardcast message to both clients', function(done) {
			let clientReceive = new Promise((resolve, reject) => {
				client.on('broadcast', res => {
					assert.deepEqual(res.body, {
						success : 1
					});
					resolve();
				});
			});
			let anotherClientReceive = new Promise((resolve, reject) => {
				anotherClient.on('broadcast', res => {
					assert.deepEqual(res.body, {
						success : 1
					});
					resolve();
				});
			});

			Promise.all([clientReceive, anotherClientReceive]).then(() => done());

			client.emit('broadcast');
		});

		it('should allow client access to REST API', function(done) {
			client.on('response:getUser', res => {
				assert.deepEqual(res.body, {
					context : 'controller',
					user : store.users['1']
				});
				done();
			});
			client.emit('GET /admin/users/1', {}, { responseEvent : 'response:getUser'});
		});

		it('should able to catch error', function(done) {
			client.on('Error', res => {
				assert.equal(res.status, 500);
				done();
			});
			client.emit('testError');
		})

	});
	describe('REST API', function() {
		it('GET admin/users', function() {
			return fetch('https://localhost:13456/admin/users', {
				method : 'GET',
				agent : new https.Agent({
					rejectUnauthorized : false
				})
			}).then(res => {
				assert.equal(200, res.status);
				return res.json();
			}).then(json => {
				assert.deepEqual(json, store.users)
			});
		});
		it('GET /projects/2/reports/2', function() {
			return fetch('https://localhost:13456/projects/2/reports/2', {
				method : 'GET',
				agent : new https.Agent({
					rejectUnauthorized : false
				})
			}).then(res => {
				assert.equal(200, res.status);
				return res.json();
			}).then(json => {
				assert.deepEqual(json, store.projects['2'].reports['2']);
			});
		});

		it('GET /projects/2/files/1', function() {
			return fetch('https://localhost:13456/projects/2/files/1', {
				method : 'GET',
				agent : new https.Agent({
					rejectUnauthorized : false
				})
			}).then(res => {
				assert.equal(200, res.status);
				return res.json();
			}).then(json => {
				assert.deepEqual(json, store.projects['2'].files['1']);
			});
		});
	});
	after(function() {
		app.webService.stopWebService();
	});
});
