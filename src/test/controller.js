import { Mixin } from 'mixwith';
import { Route, ALL, GET } from '../decorators';
import store     from './store';
import views     from 'koa-views';

const Admin = Route('*', '/admin').http(false).REST().realtime();
const User  = Admin.Route('*', '/users');
const Project = Route('*', '/projects/:projectId').http(false).REST().realtime()

export default Mixin(baseClass => class extends baseClass {

    constructor(...args) {
        super(...args);
        this.context = 'controller';
    }

    @ALL('*').REST().use()
    viewEngine = views(__dirname, {
        map : { 'mustache' : 'mustache' },
        extension : 'mustache'
    })

    @GET('/testView').REST().use()
    testView = async (ctx, next) => {
        await ctx.render('testView');
    }

    @Admin.GET('/users/:userId')
        .pre(ctx => {
            return [ctx.state.routeParams.userId]
        })
        .post(function(ctx, user, next) {
            ctx.body = { user, context : this.context };
            return next();
        })
        .use()
    getUserById = id => {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve(store.users[id]);
            }, 100);
        });
    }

    @User.GET('/').post((ctx, users, next) => {
        ctx.body = users;
        return next();
    }).use()
    get listUsers() {
        return async () => {
            return await Promise.resolve(store.users);
        }
    }

    @Project.GET('/reports/:reportId').pre(ctx => [ctx.state.routeParams.projectId, ctx.state.routeParams.reportId]).post((ctx, report, next) => {
        ctx.body = report;
        return next();
    }).use()
    getReport(projectId, reportId) {
        return store.projects[projectId].reports[reportId];
    }

    @Project.Route({
        paths : '!/reports !/reports/*',
        routePattern : '/:resource/:resourceId'
    }).use()
    getResource(ctx, next) {
        let { resource, resourceId, projectId } = ctx.state.routeParams;
        ctx.body = store.projects[projectId][resource][resourceId];
        return next();
    }

    @Route('joinRoom').realtime().use()
    joinRoom(ctx, next) {
        let room = ctx.request.body.room;
        ctx.client.join(room, () => {
            ctx.body = {
                success : 1
            }
            return next();
        });
    }

    @Route('broadcast').realtime().use()
    broadcast(ctx, next) {
        ctx.end();
        ctx.client.nsp.to('/').emit('broadcast', {body : { success : 1}});
        return next();
    }

    @Route('testError').realtime().use()
    testError(ctx, next) {
        throw Error('Test Error');
    }

});
