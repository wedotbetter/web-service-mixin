import { WebServiceMixin, Route, BeforeRealtime } from '../index';
import Controller from './controller';

export default class App {
    constructor(webServiceOptions) {
        this.context = 'app';
        this.webService = new (WebServiceMixin(class {}))();
        this.controller = new (Controller(class {}))();
        this.webService
            .config(webServiceOptions)
            .import(this, this.controller)
            .initWebService();
    }

    @BeforeRealtime().use()
    method(client, next) {
        client.state = client.state || {}
        client.state.method = {
            token : (client.request.query || client.request._query).token
        };
        return next();
    }

    @BeforeRealtime().use()
    initializer = (client, next) => {
        client.state = client.state || {}
        client.state.initializer = {
            token : (client.request.query || client.request._query).token
        };
        return next();
    }

    @BeforeRealtime().use()
    get getter() {
        return function(client, next) {
            client.state = client.state || {}
            client.state.getter = {
                token : (client.request.query || client.request._query).token
            };
            return next();
        }
    }

    @Route('GET', '/beforeConnectState').realtime().use()
    getBeforeConnectState(ctx, next) {
        ctx.body = ctx.client.state;
        return next();
    }
}
