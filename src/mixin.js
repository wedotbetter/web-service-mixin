import { Mixin }    from 'mixwith';
import _            from 'lodash';
import Koa          from 'koa';
import Promise      from 'bluebird';
import http         from 'http';
import https        from 'https';
import redisAdapter from 'socket.io-redis';
import Debug        from 'debug';
import redis        from 'redis';
import parseOptions from 'args-options';

import SocketIO            from './socket.io';
import Context             from './context';
import { WebServiceError } from './errors';

const debug = Debug('web-service');

export default Mixin(baseClass => class WebService extends baseClass {
    constructor(...args) {
        super(...args);
        this.webServiceOptions = {
            http     : null,
            https    : null,
            cache    : null,
            REST     : null,
            koa      : null,
            realtime : null
        };
    }

    config(options) {
        _.forEach(options, (option, key) => {
            this[key](option);
        });
        return this;
    }

    import(...sources) {

        _.forEach(sources, target => {
            let obj = target;
            do {
                let keys = Object.getOwnPropertyNames(obj);

                _.forEach(keys, key => {
                    _.forEach(['REST', 'realtime', 'beforeRealtime'], type => {
                        if (key.startsWith(`${type}:`) && !key.endsWith(':order')) {
                            this.webServiceOptions[type] = this.webServiceOptions[type] || {};
                            this.webServiceOptions[type].middlewares = this.webServiceOptions[type].middlewares || [];
                            this.webServiceOptions[type].middlewares.push({
                                fn : target[key].bind(target),
                                order : target[`${key}:order`]
                            });
                        }
                    });
                });

            } while (obj = Object.getPrototypeOf(obj));

        });

        return this;
    }

    @parseOptions(['http.port', 'http.host'])
    http(options) {
        this.webServiceOptions.http = _.merge(this.webServiceOptions.http, options)
        return this;
    }

    @parseOptions(['https.cred', 'https.port', 'https.host'])
    https(options) {
        this.webServiceOptions.https = _.merge(this.webServiceOptions.https, options)
        return this;
    }

    redis(options = {}) {
        this.webServiceOptions.redis = _.merge(this.webServiceOptions.redis, options);
        return this;
    }

    REST(options = {}) {
        this.webServiceOptions.REST = _.merge(this.webServiceOptions.REST, options);
        return this;
    }

    realtime(options = {}) {
        this.webServiceOptions.realtime = _.merge(this.webServiceOptions.realtime, options);
        return this;
    }

    configKoa() {
        let koa = new Koa();
        this.useMiddlewares(koa, this.webServiceOptions.REST.middlewares);
        return koa;
    }

    configREST() {
        let { REST } = this.webServiceOptions;
        _.forEach(['http', 'https'], protocol => {
            let server = this[`${protocol}Server`];
            if (server) {
                let handlerName = `${protocol}REST`
                this[handlerName] = this.configKoa();
                server.on('request', this[handlerName].callback())
            }
        });
    }

    configSocketIO() {
        let { realtime, beforeRealtime, redis } = this.webServiceOptions;
        let socketIO = new SocketIO(realtime);

        if (redis && !realtime.adapter) {
            socketIO.adapter(redisAdapter(redis));
        }

        if (realtime.adapter) {
            socketIO.adapter(realtime.adapter);
        }

        this.useMiddlewares(socketIO, beforeRealtime.middlewares);
        socketIO.on('connection', socket => {
            this.useMiddlewares(socket, realtime.middlewares);
        });

        return socketIO;
    }

    configRealtime() {
        let { realtime, redis } = this.webServiceOptions;
        _.forEach(['http', 'https'], protocol => {
            let server = this[`${protocol}Server`];
            if (server)
                this[`${protocol}Realtime`] = this.configSocketIO().attach(server);
        });
    }

    useMiddlewares(target, middlewares) {
        _.forEach(_.sortBy(middlewares, 'order'), middleware => {
            target.use(middleware.fn);
        });
    }

    initWebService() {
        let { realtime, REST, cache, http : httpConfig, https : httpsConfig } = this.webServiceOptions;

        if (httpConfig)
            this.httpServer = http.createServer();

        if (httpsConfig && httpsConfig.cred)
            this.httpsServer = https.createServer(httpsConfig.cred);

        if (REST)
            this.configREST();

        if (realtime)
            this.configRealtime();

        return this;
    }

    startWebService() {
        _.forEach(['http', 'https'], protocol => {
            if (!this.webServiceOptions[protocol])
                return;
            let { port, host, backlog, cb } = this.webServiceOptions[protocol];
            this[`${protocol}Server`].listen(port, host, backlog, cb);
        });
    }

    stopWebService() {
        _.forEach(['http', 'https'], protocol => {
            if (!this[`${protocol}Server`])
                return;
            this[`${protocol}Server`].close();
        });
    }

});
