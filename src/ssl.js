import pem     from 'pem';
import fs      from 'fs';
import path    from 'path';
import Promise from 'bluebird';


export default (destination, options) => {
    return new Promise((resolve, reject) => {
        let keyPath  = path.join(destination, 'key.pem');
        let certPath = path.join(destination, 'cert.pem');
        try {
            let key  = fs.readFileSync(keyPath);
            let cert = fs.readFileSync(certPath);
            resolve({ key, cert });
        } catch(err) {
            pem.createCertificate(options, (err, { serviceKey, certificate }) => {
                if (err) reject(err);

                let key  = serviceKey;
                let cert = certificate;

                fs.writeFileSync(keyPath, serviceKey);
                fs.writeFileSync(certPath, certificate);

                resolve({key, cert});
            });
        }
    });
}
